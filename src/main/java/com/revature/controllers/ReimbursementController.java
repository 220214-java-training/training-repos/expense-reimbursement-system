package com.revature.controllers;

import com.revature.models.Reimbursement;
import com.revature.models.ReimbursementStatus;
import com.revature.services.ReimbursementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ReimbursementController {

    @Autowired
    private ReimbursementService reimbursementService;

    @RequestMapping(value = "/reimbursements", method = RequestMethod.GET)
    public List<Reimbursement> handleGetReimbursements(
            @RequestParam(value = "status",required = false)ReimbursementStatus status){
        System.out.println(status);
        return reimbursementService.getReimbursements(status);
    }

    @RequestMapping(value="/employees/{id}/reimbursements", method = RequestMethod.POST)
    public ResponseEntity<Reimbursement> handlePostReimbursement(
            @Valid @RequestBody Reimbursement reimbursement,
            @PathVariable("id")int employeeId){
        Reimbursement newReimbursement = reimbursementService.createReimbursement(reimbursement, employeeId);
        if(newReimbursement==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(newReimbursement, HttpStatus.CREATED);
    }

}
