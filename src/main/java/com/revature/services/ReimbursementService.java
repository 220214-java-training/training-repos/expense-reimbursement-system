package com.revature.services;

import com.revature.models.Employee;
import com.revature.models.Reimbursement;
import com.revature.models.ReimbursementStatus;
import com.revature.repositories.EmployeeRepository;
import com.revature.repositories.ReimbursementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReimbursementService {

    @Autowired
    private ReimbursementRepository reimbursementRepo;

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Reimbursement> getReimbursements(ReimbursementStatus status){
        if(status==null){  // no status was provided
            return reimbursementRepo.findAll();
        } else { // status is a valid ReimbursementStatus
            return reimbursementRepo.findReimbursementsByStatus(status);
        }
    }

    @Transactional // adding the @Transactional annotation defines this method as executing in the scope of a tx
    public Reimbursement createReimbursement(Reimbursement reimbursement, int emplId){
        // establish the relationship between the reimbursement and the relevant employee
        // to do this, we need to add the new reimbursement to the employee's existing list of reimbursements

        // obtain existing employee
        Optional<Employee> employeeOptional = employeeRepository.findById(emplId);
        if(employeeOptional.isPresent()){

            // save the reimbursement to the db if there is a valid employee
            Reimbursement savedReimbursement = reimbursementRepo.save(reimbursement);

            Employee e = employeeOptional.get();
            List<Reimbursement> existingReimbursements = e.getReimbursements();
            existingReimbursements.add(savedReimbursement);

            // once we update the employee in the database (containing the new reimbursement), the relationship will be
            // established
            employeeRepository.save(e);
            return reimbursement;
        } else {
            // no valid employee in the db w provided ID
            return null;
        }
    }

}
