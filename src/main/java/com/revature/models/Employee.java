package com.revature.models;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String password;
    private String first;
    private String last;
    private boolean isManager;
    @OneToMany
    @JoinColumn(name="employee_id")
    private List<Reimbursement> reimbursements;

    public Employee() {
    }

    public Employee(int id, String first, String last, String username, String password, boolean isManager,
                    List<Reimbursement> reimbursements) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.first = first;
        this.last = last;
        this.isManager = isManager;
        this.reimbursements = reimbursements;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean manager) {
        isManager = manager;
    }

    public List<Reimbursement> getReimbursements() {
        return reimbursements;
    }

    public void setReimbursements(List<Reimbursement> reimbursements) {
        this.reimbursements = reimbursements;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id && isManager == employee.isManager && Objects.equals(username, employee.username) && Objects.equals(password, employee.password) && Objects.equals(first, employee.first) && Objects.equals(last, employee.last) && Objects.equals(reimbursements, employee.reimbursements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, first, last, isManager, reimbursements);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", first='" + first + '\'' +
                ", last='" + last + '\'' +
                ", isManager=" + isManager +
                ", reimbursements=" + reimbursements +
                '}';
    }
}
