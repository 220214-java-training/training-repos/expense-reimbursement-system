package com.revature.repositories;

import com.revature.models.Reimbursement;
import com.revature.models.ReimbursementStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReimbursementRepository extends JpaRepository<Reimbursement, Integer> {

    List<Reimbursement> findReimbursementsByStatus(ReimbursementStatus status);

    // sql = select * from reimbursement where status = PENDING;
    // jpql = select r from Reimbursement r where status = PENDING
    @Query("select r from Reimbursement r where status = PENDING")
    List<Reimbursement> getPendingReimbursements();

    @Query("select r from Reimbursement r where status = APPROVED or status = DENIED")
    List<Reimbursement> getResolvedReimbursements();
}
